import React from 'react';
import ImagePreview from './ImagePreview';
import ImageRep from './model/ImageRep';

const GridView: React.FC<{ files: ImageRep[] }> = ({ files }) => {
    var images = files.map((element) => {
        return (<ImagePreview title={element.name} file={element.file} key={element.id}></ImagePreview>);
    });

    if (images.length > 0) {
        return (
            <div className='GridView'>
                {images}
            </div>
        );
    }
    else {
        return (
            <div className='GridView'>
                <h2>Empty list</h2>
            </div>
        );
    }
}

export default GridView;