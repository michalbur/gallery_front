class ImageRep {
    id: string;
    name: string;
    file: string;
    constructor(id: string, name: string, file: string) {
        this.id = id;
        this.name = name;
        this.file = file;
    }
}

export default ImageRep;