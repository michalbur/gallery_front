import React, { useState, FormEvent } from 'react';
import { Button, FormGroup, FormControl, FormLabel } from 'react-bootstrap';

const LoginColumn: React.FC = () => {

    const [login, setLogin] = useState("");
    const [password, setPassword] = useState("");

    function submitHandler(event: FormEvent) {
        event.preventDefault();
        console.log(login + " " + password + " " + login.length);
        setLogin("");
        setPassword("");
    }

    return (
        <div className="LoginColumn">
            <form onSubmit={submitHandler}>
                <FormGroup controlId="login">
                    <FormLabel>Login</FormLabel>
                    <FormControl
                        autoFocus
                        type="login"
                        value={login}
                        onChange={(
                            ev: React.ChangeEvent<HTMLInputElement>,
                        ): void => setLogin(ev.target.value)}
                    />
                </FormGroup>
                <FormGroup controlId="password">
                    <FormLabel>Password</FormLabel>
                    <FormControl
                        value={password}
                        type="password"
                        onChange={(
                            ev: React.ChangeEvent<HTMLInputElement>,
                        ): void => setPassword(ev.target.value)}
                    />
                </FormGroup>
                <Button block type="submit">
                    Login
                </Button>
            </form>
        </div>
    );
}

export default LoginColumn;