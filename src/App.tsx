import React from 'react';
import LoginColumn from './LoginColumn'
import MainColumn from './MainColumn'
import './App.css';

const App: React.FC = () => {
  return (
    <div className="App">
      <LoginColumn></LoginColumn>
      <MainColumn></MainColumn>
    </div>
  );
}

export default App;
