import React, { useState, useEffect } from 'react';
import ImageRep from './model/ImageRep';
import FileSender from './FileSender';
import GridView from './GridView';;

const MainColumn: React.FC = () => {
    const [files, setFiles] = useState<ImageRep[]>([]);
    const [uploadsCount, setUploadsCount] = useState<number>(0);
    
    useEffect(() => {
        fetch('http://192.168.0.160:8000/getall', { mode: "cors" })
            .then(respone => { 
                return respone.json() 
            })
            .then(js_resp => {
                console.log(js_resp);
                setFiles(js_resp);
            })
            .catch(err => console.log(err))
    }, [uploadsCount]);

    return (
        <div className='MainColumn'>
            <FileSender onImageAdd={setUploadsCount}></FileSender>
            <GridView files={files}></GridView>
        </div>
    );
}

export default MainColumn;