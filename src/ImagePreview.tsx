import React from 'react';
import styles from './ImagePreview.module.css';

const ImagePreview: React.FC<{ title: string, file: string }> = ({title, file}) => {
    return (
        <div className='ImagePreview'>
            <a href={"http://192.168.0.160:8000/"+file}>
                <img src={"http://192.168.0.160:8000/"+file} alt={title} className={styles.imgpr}/>
            </a>
            <span className={styles.title}>{title}</span>
        </div>
    );
}

export default ImagePreview;