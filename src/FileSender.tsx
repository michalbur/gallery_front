import React, { useState, FormEvent, Dispatch, SetStateAction } from 'react';
import { FormGroup, FormLabel, FormControl, Button } from 'react-bootstrap';
import styles from './FileSender.module.css';

const FileSender: React.FC<{onImageAdd: Dispatch<SetStateAction<number>>}> = ({onImageAdd}) => {
    const [f_name, setF_name] = useState("");
    const [f_watermark, setF_watermark] = useState("");
    const [file, setFile] = useState<File>(new File([], ""));

    function submitHandler(event: FormEvent) {
        event.preventDefault();
        console.log(f_name + " " + f_watermark);

        const formData = new FormData();
        formData.append("my_file", file);
        formData.append("watermark", f_watermark);
        formData.append("fileName", f_name);
        fetch("http://192.168.0.160:8000/upload", {
            method: 'POST',
            mode: "cors",
            body: formData
        })
        .then(response => {
            onImageAdd(prev => prev + 1);
            return response.json();
        })
        .catch(err => console.error(err));

        setF_watermark("")
        setF_name("");
        setFile(new File([], ""));
    }

    return (
        <div className='FileSender'>
            <form onSubmit={submitHandler}>
                <FormGroup controlId="f_name" className={styles.tightForm}>
                    <FormLabel>File name</FormLabel>
                    <FormControl
                        type="text"
                        value={f_name}
                        onChange={(
                            ev: React.ChangeEvent<HTMLInputElement>
                        ): void => setF_name(ev.target.value)}
                        required
                    />
                </FormGroup>
                <FormGroup controlId="f_watermark" className={styles.tightForm}>
                    <FormLabel>Watermark</FormLabel>
                    <FormControl
                        type="text"
                        value={f_watermark}
                        onChange={(
                            ev: React.ChangeEvent<HTMLInputElement>
                        ): void => setF_watermark(ev.target.value)}
                        required
                    />
                </FormGroup>
                <FormGroup controlId="file" className={styles.tightForm}>
                    <FormLabel>File</FormLabel>
                    <FormControl
                        type="file"
                        accept="image/*"
                        onChange={(ev: React.ChangeEvent<HTMLInputElement>): void => {
                            if (ev.target.files != null) {
                                setFile(ev.target.files[0]);
                            }
                        }}
                        required
                    />
                </FormGroup>
                <Button block type="submit" className={styles.tightForm}>
                    Send
                </Button>
            </form>
        </div>
    );
}

export default FileSender;